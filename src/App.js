import './App.css';
import {Route, withRouter} from "react-router-dom";
import HeaderContainer from './components/Header/HeaderContainer';
import Nav from './components/Nav/Nav';
import MessageContainer from "./components/Message/MessageContainer";
import Photo from "./components/Photo/Photo"
import Music from "./components/Music/Music";
import Video from "./components/Video/Video";
import Friends from "./components/Friends/Friends";
import UsersContainer from "./components/Users/UsersContainer";
import ProfileContainer from "./components/Profile/ProfileContainer";
import Login from "./components/Login/Login";
import {connect} from "react-redux";
import {logoutThunkCreator} from "./redux/auth-reducer";
import {compose} from "redux";
import React from 'react'
import {initializeApp} from "./redux/app-reducer";
import Preloader from "./components/Common/Preloader/Preloader";

class App extends React.Component {
    componentDidMount(){
        this.props.initializeApp()
    }


    render() {
        if(!this.props.initialized) {
            return <Preloader/>
        }
        return (
            <div className='app-wrapper'>
                <HeaderContainer/>
                {this.props.isAuth ? <Nav/>:''}

                <div className="content">
                    <Route path='/login' render={() => <Login/>}/>
                    <Route path='/profile/:userId?' render={() => <ProfileContainer/>}/>
                    <Route path='/users' render={() => <UsersContainer/>}/>
                    <Route path='/friends' component={Friends}/>
                    <Route path='/dialogs' render={() => <MessageContainer/>}/>
                    <Route path='/photo' component={Photo}/>
                    <Route path='/music' component={Music}/>
                    <Route path='/video' component={Video}/>
                </div>
            </div>
        );
    }
}
const mapStateToProps = (state) =>({
    initialized:state.app.initialized,
    isAuth: state.auth.isAuth
})
export default compose(
    withRouter,
    connect(mapStateToProps, {initializeApp,logoutThunkCreator}))(App);


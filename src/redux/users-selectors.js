export const getUsers = (state) =>{
    return state.users
}
export const getPageSize = (state) =>{
    return state.pageSize
}
export const getTotalUsersCount = (state) =>{
    return state.totalUsersCount
}
export const getTargetPage = (state) =>{
    return state.targetPage
}
export const getIsFetching = (state) =>{
    return state.isFetching
}
export const getFollowingInProgress = (state) =>{
    return state.followingInProgress
}
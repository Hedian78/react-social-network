import {authMe, login, logout} from "../api/api";

const SET_USER_DATA = 'SET_USER_DATA';


let initialState = {
    id: null,
    email: null,
    login: null,
    isFetching: false,
    isAuth: false
}


const authReducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_USER_DATA:
            return {
                ...state,
                isAuth: action.isAuth,
                ...action.payload

            }
        default: return state;
    }
}


export const setUserData = (id, login, email, isAuth) => ({type:SET_USER_DATA, payload: {id, login, email, isAuth}})

/*ThunkCreators*/
export const authThunkCreator = () => (dispatch) =>{
    return authMe().then(data => {
        if (data.resultCode === 0){
            let {id, login, email} = data.data;
            dispatch(setUserData(id, login, email, true));
        }
    });

}

export const loginThunkCreator = (email, password) => (dispatch) =>{
    login(email, password).then(data => {
        if (data.resultCode === 0){
            dispatch(authThunkCreator());

        }
    });
}
export const logoutThunkCreator = () => (dispatch) =>{
    logout().then(response => {
        if (response.data.resultCode === 0){
            dispatch(setUserData(null, null, null, false));
        }
    });
}

export default authReducer
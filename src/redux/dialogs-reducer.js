const SEND_MESSAGE ='SEND_MESSAGE';
const UPDATE_NEW_MESSAGE_TEXT = 'UPDATE_NEW_MESSAGE_TEXT';


let initialState = {
    contacts:[
        {id: 1, name: "Anton"},
        {id: 2, name: "Andrew"},
        {id: 3, name: "Mary"},
        {id: 4, name: "Tony"},
        {id: 5, name: "Mr. Anderson"}
    ],
    messages:[
        {id: 1, message: 'Hi'},
        {id: 2, message: 'Hi'},
        {id: 3, message: 'How are you?'},
        {id: 4, message: 'Not bad'},
        {id: 5, message: 'It`s cool!'},
    ],
    newMessageText: ""
};

const dialogsReducer = (state = initialState, action) =>{
    switch (action.type){
        case UPDATE_NEW_MESSAGE_TEXT: {
            return {
                ...state,
                newMessageText: action.text
            };

        }
        case SEND_MESSAGE: {
            let newTextMessage = state.newMessageText;

            return {
                ...state,
                newMessageText: "",
                messages: [...state.messages,{id: 6,message: newTextMessage}]
            };
        }
        default:
            return state;
    }
}

export const sendMessageActionCreator = () =>({ type : SEND_MESSAGE})
export const updateNewMessageCreator = (text) =>
    ({type : UPDATE_NEW_MESSAGE_TEXT, text : text})


export default dialogsReducer;
import dialogsReducer from "./dialogs-reducer";
import profileReducer from "./profile-reducer";



let store = {
    _state: {
        post: [
            {id: 1, text: "Hello world!"},
            {id: 2, text: "what??This is amaizing"},
            {id: 3, text: "My first post"}
        ],
        dialogsData:
            {
                contacts:[
                    {id: 1, name: "Anton"},
                    {id: 2, name: "Andrew"},
                    {id: 3, name: "Mary"},
                    {id: 4, name: "Tony"},
                    {id: 5, name: "Mr. Anderson"}
                ],
                messages:[
                    {id: 1, message: 'Hi'},
                    {id: 2, message: 'Hi'},
                    {id: 3, message: 'How are you?'},
                    {id: 4, message: 'Not bad'},
                    {id: 5, message: 'It`s cool!'},
                ],
                newMessageText: ""
            }



    },

    getState(){
      return this._state;
    },
    subscribe(observer){
        this._rerenderEntireTree = observer; //observer
    },

    _rerenderEntireTree() {
        console.log('state');
    },
    dispatch(action){
        this._state.post = profileReducer(this._state.post, action);
        this._state.dialogsData = dialogsReducer(this._state.dialogsData, action);

        this._rerenderEntireTree(this._state);
    }

}




export default store;
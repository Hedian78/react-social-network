import {follow, getUsers, unfollow} from "../api/api";

const FOLLOW = 'FOLLOW';
const UNFOLLOW = 'UNFOLLOW';
const SET_USERS = 'SET_USERS';
const SET_TARGET_PAGE = 'SET_TARGET_PAGE';
const SET_COUNT_USERS = 'SET_COUNT_USERS';
const TOGGLE_IS_FETCHING = 'TOGGLE_IS_FETCHING';
const TOGGLE_IS_FOLLOWING_PROGRESS = 'TOGGLE_IS_FOLLOWING_PROGRESS';




let initialState = {
    users:[],
    pageSize: 50,
    totalUsersCount: 0,
    targetPage: 1,
    isFetching: false,
    followingInProgress: []
}

const userReducer = (state = initialState, action) =>{
    switch(action.type){
        case FOLLOW:
            return {
                ...state,
                users: state.users.map(user =>{
                    if(user.id === action.userId){
                        return {...user, follow: true};
                    }
                        return user;
                })
            }
        case UNFOLLOW:
            return {
                ...state,
                users: state.users.map(user =>{
                    if(user.id === action.userId){
                        return {...user, follow: false};
                    }
                    return user;
                })
            }
        case SET_USERS:
                return {...state, users: action.users};
        case SET_TARGET_PAGE:
                return{...state,targetPage: action.numberPage}
        case SET_COUNT_USERS:
                return{...state,totalUsersCount: action.countUsers / 10}//fix count
        case TOGGLE_IS_FETCHING:
                return{...state,isFetching: action.isFetching}
        case TOGGLE_IS_FOLLOWING_PROGRESS:
            return{...state,
                followingInProgress: action.isFetching
                    ? [...state.followingInProgress, action.userId]
                    : state.followingInProgress.filter(id => id != action.userId)
            }
        default:
            return state;

    }
}

export const followAC = (userId) => ({type: FOLLOW, userId})
export const unfollowAC = (userId) => ({type: UNFOLLOW, userId})
export const setUsersAC = (users) => ({type:SET_USERS, users})
export const setTargetPageAC = (numberPage) =>({type:SET_TARGET_PAGE, numberPage})
export const setCountUsersAC = (countUsers) =>({type:SET_COUNT_USERS,countUsers})
export const toggleIsFetchingAC = (isFetching) =>({type:TOGGLE_IS_FETCHING,isFetching})
export const toggleIsFollowingProgressAC = (isFetching,userId) =>({type:TOGGLE_IS_FOLLOWING_PROGRESS, isFetching, userId})

/*ThunkCreators*/
export const getUsersThunkCreator =(targetPage,pageSize) => (dispatch) =>{
    dispatch(toggleIsFetchingAC(true));
    getUsers(targetPage, pageSize) .then(data => {
        dispatch(toggleIsFetchingAC(false));
        dispatch(setUsersAC(data.items));
        dispatch(setCountUsersAC(data.totalCount));
    });
}

export const followThunkCreator =(id) => (dispatch) =>{
    dispatch(toggleIsFollowingProgressAC(true, id));
    follow(id)
        .then(data => {
            if(data.resultCode == 0){
                dispatch(followAC(id))
            }
            dispatch(toggleIsFollowingProgressAC(false, id));
        });

}
export const unfollowThunkCreator = (id) => (dispatch) =>{
    dispatch(toggleIsFollowingProgressAC(true, id));
    unfollow(id)
        .then(data => {
            if(data.resultCode == 0){
                dispatch(unfollowAC(id))
            }
            dispatch(toggleIsFollowingProgressAC(false, id));
        });
}




export default userReducer;
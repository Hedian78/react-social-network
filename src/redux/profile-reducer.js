import {getProfile, getStatus, updateStatus} from "../api/api";

const ADD_POST = 'ADD-POST';
const UPDATE_NEW_POST_TEXT = 'UPDATE_NEW_POST_TEXT';
const GET_PERSONAL_USER_PAGE  = 'GET_PERSONAL_USER_PAGE';
const SET_STATUS = 'SET_STATUS';


let initialState = {
    usersProfile: null,
    post: [
    {id: 1, text: "Hello world!"},
    {id: 2, text: "what??This is amaizing"},
    {id: 3, text: "My first post"}
    ],
    newPost: "",
    status: ""
}

const profileReducer = (state = initialState, action) =>{
    switch (action.type){
        case UPDATE_NEW_POST_TEXT:
            return {
                ...state,
                newPost: action.text
            }
        case ADD_POST:
            let newTextPost = state.newPost;
            return {
                ...state,
                newPost: "",
                post: [...state.post, {id: 5, text: newTextPost}]
            }
        case GET_PERSONAL_USER_PAGE:
            return{...state, usersProfile: action.user}
        case SET_STATUS:
            return{...state, status: action.status}

        default:
            return state;
    }

}


export const addPostActionCreator = () =>({ type : ADD_POST})
export const updateNewPostCreator = (text) =>
    ({type : UPDATE_NEW_POST_TEXT, text : text})
export const getPersonalUserPage = (user)=>({type: GET_PERSONAL_USER_PAGE, user})
export const setStatus = (status)=>({type: SET_STATUS, status})



/*ThunkCreators*/
export const getUserPageThunkCreator =(userId) => (dispatch) =>{
    getProfile(userId).then(data => {
        dispatch(getPersonalUserPage(data));
    });
}

export const getStatusThunkCreator =(userId) => (dispatch) =>{
    getStatus(userId).then(response => {
        dispatch(setStatus(response.data));
    });
}

export const updateStatusThunkCreator =(status) => (dispatch) =>{
    updateStatus(status).then(response => {
        if(response.data.resultCode === 0){
            dispatch(setStatus(status));
        }
    });
}



export default profileReducer;
import {authThunkCreator} from "./auth-reducer";

const SET_INITIALIZED = 'SET_INITIALIZED';



let initialState = {
    initialized: false
}


const appReducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_INITIALIZED:
            return {
                ...state,
                initialized: true
            }
        default: return state;
    }
}


export const setInitialized = (data) => ({type:SET_INITIALIZED})


/*ThunkCreators*/
export const initializeApp = () => (dispatch) =>{
    let promise = dispatch(authThunkCreator());
    promise.then(()=>{
        dispatch(setInitialized());
    })

}

export default appReducer
import * as axios from "axios";

const instance = axios.create({
    withCredentials: true,
    baseURL: 'https://social-network.samuraijs.com/api/1.0/',
    headers:{
        "API-KEY": "5e31b439-cb50-4178-b08d-433f2470fc3a"
    }
})

export const getUsers = (targetPage = 1, pageSize = 10) =>{
    return instance.get(`users?page=${targetPage}&count=${pageSize}`)
        .then(response => response.data);
}

export const follow = (id) =>{
    return instance.post(`follow/${id}`)
        .then(response => response.data);
}
export const unfollow = (id) =>{
    return instance.delete(`follow/${id}`)
        .then(response => response.data);
}
export const getProfile = (userId) =>{
    return instance.get(`profile/${userId}`)
        .then(response => response.data);
}

export const authMe = () =>{
    return instance.get(`auth/me`)
        .then(response => response.data);
}

export const getStatus = (userId) =>{

    return instance.get(`profile/status/` + userId)
}
export const updateStatus = (status) =>{
    return instance.put(`profile/status`, {status: status})
}

export const login = (email, password, rememberMe = false) =>{
    return instance.post('auth/login', {
            email: email,
            password: password
    }).then(response => response.data);
}
export const logout = () =>{
    return instance.delete(`auth/login`)
}

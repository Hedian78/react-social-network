import React, {useState, useEffect} from "react"
import Profile_s from '../Profile.module.css';


const ProfileStatusWithHooks = (props)=>{
    let [editMode,setEditMode] = useState(false);
    let [status, setStatus] = useState(props.status);

    useEffect(()=>{
        setStatus(props.status)
    }, [props.status])

    const activateEditMode = () =>{
        setEditMode(true);
    }
    const deactivateEditMode = () =>{
        setEditMode(false);
        props.updateStatus(status)
    }
    const onStatusChange = (e) =>{
        setStatus(e.currentTarget.value);
    }
    return (
        <div className={Profile_s.status}>
            {!editMode &&
            <div>
                <span onDoubleClick={activateEditMode}
                      className={Profile_s.status_span}>{props.status}</span>
            </div>
            }
            {editMode &&
            <div>
                <input autoFocus={true}
                       onBlur={deactivateEditMode}
                       value={status}
                       onChange={onStatusChange}/>
            </div>
            }
        </div>
    )

}

export default ProfileStatusWithHooks
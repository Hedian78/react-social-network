import React from "react"
import Profile_s from '../Profile.module.css';


class ProfileStatus extends React.Component{
    state = {
        editMode: false,
        status: this.props.status
    }
    activateEditMode = () => {

        this.setState({
            editMode: true
        });
    }
    deactivateEditMode = ()=>{
        this.setState({
            editMode: false
        });
        this.props.updateStatus(this.state.status);
    }
    onStatusChange = (e) =>{
       this.setState({
           status: e.currentTarget.value
       })

    }
    componentDidUpdate(prevProps, prevState){
        /*Условие обязательно*/
        if(prevProps.status !== this.props.status) {
            this.setState({
                status: this.props.status
            })
        }
    }

    render(){
    return (
        <div className={Profile_s.status}>
            {!this.state.editMode &&
            <div>
                <span onDoubleClick={this.activateEditMode}
                      className={Profile_s.status_span}>{this.props.status}</span>
            </div>
            }
            {this.state.editMode &&
            <div>
                <input autoFocus={true}
                       onBlur={this.deactivateEditMode.bind(this)}
                       value={this.state.status}
                       onChange={this.onStatusChange}/>
            </div>
            }
        </div>
    )}

}

export default ProfileStatus
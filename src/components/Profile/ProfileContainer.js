import React from 'react';
import Profile from "./Profile";
import {connect} from "react-redux";
import {withRouter} from "react-router-dom";
import {getStatusThunkCreator, getUserPageThunkCreator, updateStatusThunkCreator} from "../../redux/profile-reducer";
import {withAuthRedirect} from "../../hoc/AuthRedirect";
import {compose} from "redux";

class ProfileContainer extends React.Component {

    componentDidMount(){
        if(this.props.match.params.userId) {

            this.props.getUserPageThunkCreator(this.props.match.params.userId);
            this.props.getStatusThunkCreator(this.props.match.params.userId);
        }
        else {

            this.props.getUserPageThunkCreator(this.props.myId);
            this.props.getStatusThunkCreator(this.props.myId);
        }

    }

    render() {

        return (
            <Profile {...this.props} status={this.props.status} updateStatus={this.props.updateStatusThunkCreator}/>

        )
    }
}


let mapStateToProps = (state) => ({
    profile: state.ProfilePage.usersProfile,
    isAuth: state.auth.isAuth,
    myId: state.auth.id,
    status: state.ProfilePage.status
})

export default compose(
    connect(mapStateToProps,{getUserPageThunkCreator,getStatusThunkCreator,updateStatusThunkCreator}),
    withRouter,
    withAuthRedirect
)(ProfileContainer)


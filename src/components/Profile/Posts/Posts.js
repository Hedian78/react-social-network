import React, {PureComponent} from 'react';
import Posts_s from './Posts.module.css';
import Post from './Post';
import {ButtonAddPost} from "../../Common/Buttons/Button";

class Posts extends PureComponent {
   /* shouldComponentUpdate(nextProps, nextState){
        return nextProps != this.props || nextState != this.state;
    }*/
    render() {
        let PostData = this.props.posts.post.map(post => <Post post_text={post.text} key={post.id}/>);
        let newPostText = this.props.posts.newPost;
        let onNewPostChange = (e) => {
            let text = e.target.value;
            this.props.updateNewPostText(text);
        }
        let addPost = () => {
            this.props.addPost();
        }


        return <div>
            <div className={Posts_s.form}>
                <input onChange={onNewPostChange} value={newPostText} type="text" placeholder='Что у вас нового?'/>
                <ButtonAddPost addPost={this.props.addPost} value={"add"}/>
                {/*<button className={Posts_s.post_add} onClick={addPost}>add</button>*/}
            </div>
            <h2>Мои публикации</h2>
            {PostData}
        </div>;
    }
}
export default Posts;

import React from 'react';
import Posts_s from './Posts.module.css';

const Post = (props) => {
    return <div className={Posts_s.post}>
        <div className={Posts_s.logo_mini}></div>
        <div className='post_text'>{props.post_text}</div>
    </div>;
}

export default Post;

import React from 'react';
import {addPostActionCreator, updateNewPostCreator} from "../../../redux/profile-reducer";
import Posts from "./Posts";
import {connect} from "react-redux";


let mapStateToProps = (state) =>{
    return{
        posts: state.ProfilePage
    }
}
let mapDispatchToProps = (dispatch) =>{
    return{
        updateNewPostText: (text) => {
            dispatch(updateNewPostCreator(text));
        },
        addPost: () => {
            dispatch(addPostActionCreator());
        }
    }
}

const PostsContainer = connect(mapStateToProps, mapDispatchToProps)(Posts);

export default PostsContainer;

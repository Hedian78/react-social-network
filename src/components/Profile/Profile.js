import React from 'react';
import Profile_s from './Profile.module.css';
import PostsContainer from "./Posts/PostsContainer";
import Preloader from "../Common/Preloader/Preloader";
import ProfileStatus from "./ProfileStatus/ProfileStatus";
import ProfileStatusWithHooks from "./ProfileStatus/ProfileStatusWithHooks";


const Profile = (props) => {
    if(!props.profile){
       return <Preloader />
    }

    return <div className={Profile_s.content}>
        <div className={Profile_s.photo}></div>
        <div className={Profile_s.page_name}>
            <h1>{props.profile.fullName} </h1>
                <ProfileStatusWithHooks status = {props.status} updateStatus={props.updateStatusThunkCreator}/>
        </div>
        {/*<div className={Profile_s.page_info}>
            <div><b>Дата рождения:</b> 28 января 1997</div>
            <div><b>Родной город:</b> Санкт-Петербург</div>
        </div>*/}
        <div className={Profile_s.posts}>
            <PostsContainer />
        </div>
    </div>;
}

export default Profile;

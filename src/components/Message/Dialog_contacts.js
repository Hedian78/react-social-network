import Message_s from './Message.module.css'
import {NavLink} from 'react-router-dom'


const Dialog_contacts=(props)=>{

    return(
        <NavLink className={Message_s.contact_item} to={"/dialogs/" + props.id}>{props.name}</NavLink>
    )
}

export default Dialog_contacts
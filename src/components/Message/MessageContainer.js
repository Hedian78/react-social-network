import React from "react"
import {sendMessageActionCreator, updateNewMessageCreator} from "../../redux/dialogs-reducer";
import Message from "./Message";
import {connect} from "react-redux";
import {withAuthRedirect} from "../../hoc/AuthRedirect";
import {compose} from "redux";


let mapStateToProps = (state) => {
    return{
        dialogsData: state.dialogsData,
        isAuth: state.auth.isAuth
    }
}

let mapDispatchToProps = (dispatch) => {
    return{
        sendMessage: () =>{
            dispatch(sendMessageActionCreator());
        },
        onNewMessageChange: (text) =>{
            dispatch(updateNewMessageCreator(text));
        }
    }
}



export default compose(
    connect(mapStateToProps, mapDispatchToProps),
    withAuthRedirect
)(Message);

/*
let AuthRedirectComponent = withAuthRedirect(Message);
const MessageContainer = connect(mapStateToProps, mapDispatchToProps)(AuthRedirectComponent);

export default MessageContainer*/

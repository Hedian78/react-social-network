import Message_s from './Message.module.css'
import React from 'react';
import Dialog_contacts from './Dialog_contacts'
import Dialog_message from './Dialog_message'

const Message = (props) => {
    let contacts = props.dialogsData.contacts;
    let messages = props.dialogsData.messages;
    let dialogsDataContacts = contacts.map(dialog => <Dialog_contacts name={dialog.name} id={dialog.id} key={dialog.id}/>);
    let dialogsDataMessages = messages.map(mes => <Dialog_message message={mes.message} id={mes.id} key={mes.id}/>);
    let newMessageText = props.dialogsData.newMessageText;


    let sendMessage = () =>{
        props.sendMessage();
    }
    let onNewMessageChange = (e) =>{
        let text = e.target.value;
        props.onNewMessageChange(text);
    }
    return (
        <div className={Message_s.message_wrapper}>
            <div className={Message_s.contacts}>
                {dialogsDataContacts}
            </div>
            <div className={Message_s.message_block}>
                {dialogsDataMessages}
                <textarea  value={newMessageText}
                           onChange={onNewMessageChange}
                           className={Message_s.textarea_block}></textarea>
                <button onClick={sendMessage}>Send</button>
            </div>
        </div>
    );
}

export default Message
import Message_s from './Message.module.css'


let Dialog_message = (props) => {
    return (
        <div className={Message_s.message_item}>
            {props.message}
        </div>
    )
}


export default Dialog_message
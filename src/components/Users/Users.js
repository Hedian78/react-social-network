import React from 'react'
import Users_s from "./Users.module.css"
import {NavLink} from "react-router-dom";
import {follow,unfollow} from "../../api/api";

let Users = (props) =>{
    let pagesCount = Math.ceil(props.totalUsersCount / props.pageSize);
    let pages = [];
    for(let i = 1;i <= pagesCount;i++){
        pages.push(i);
    }
    return(
        <div>
            <div className={Users_s.pagination}>
                {pages.map(p => {
                    return <span onClick={()=>props.onPageChanged(p)} key={p} className={props.targetPage === p ? Users_s.selectedPage : Users_s.numberPage}> {p} </span>
                })}

            </div>            {
                props.users.map(user => <div className={Users_s.user_block} key={user.id}>
                        <NavLink to={'/profile/' + user.id}>
                            <div className={Users_s.user_personal_info}>
                                <div className={Users_s.minilogo}>
                                </div>
                                <div className={Users_s.name_user}>
                                    <h2>{user.name}</h2>
                                    <span className={Users_s.user_pageStatus}>{user.status}</span>
                                </div>
                            </div>
                        </NavLink>
                    <div className={Users_s.btn_block}>
                        {user.follow
                            ? <button disabled={props.followingInProgress.some(id=>id===user.id)}
                                      onClick={()=>{props.unfollowThunkCreator(user.id)}}
                                      className={Users_s.unfollow}>Unfollow
                            </button>
                            : <button disabled={props.followingInProgress.some(id=>id===user.id)}
                                      className={Users_s.follow}
                                      onClick={()=>{props.followThunkCreator(user.id);}}>Follow
                            </button>
                        }
                    </div>
                </div>
                )
            }
        </div>
    )
}

export default Users
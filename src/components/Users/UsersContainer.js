import React from 'react'
import {connect} from "react-redux";
import Users from "./Users";
import {setTargetPageAC, getUsersThunkCreator,
    unfollowThunkCreator, followThunkCreator} from "../../redux/user-reducer";
import Preloader from "../Common/Preloader/Preloader";
import {
    getFollowingInProgress, getIsFetching, getPageSize, getTargetPage, getTotalUsersCount,
    getUsers
} from "../../redux/users-selectors";


class UsersAPIComponent extends React.Component{

    componentDidMount(){
        this.props.getUsersThunkCreator(this.props.userPage.targetPage,this.props.userPage.pageSize);
    }
    onPageChanged = (pageNumber)=> {
        this.props.getUsersThunkCreator(pageNumber,this.props.userPage.pageSize);
        this.props.setTargetPage(pageNumber);
    }

    render(){

        return <div>

        {this.props.userPage.isFetching ? <Preloader />: null}
        <Users totalUsersCount={this.props.userPage.totalUsersCount}
                      pageSize={this.props.userPage.pageSize}
                      targetPage={this.props.userPage.targetPage}
                      users={this.props.userPage.users}
                      onPageChanged={this.onPageChanged}
                      followingInProgress={this.props.userPage.followingInProgress}
                      followThunkCreator={this.props.followThunkCreator}
                      unfollowThunkCreator={this.props.unfollowThunkCreator}
                      />
        </div>
    }
}
/*let mapStateToProps = (state) =>{
    return{
        userPage: state.users,
        pageSize: state.pageSize,
        totalUsersCount: state.totalUsersCount,
        targetPage: state.targetPage,
        isFetching: state.isFetching,
        followingInProgress: state.followingInProgress
    }
}*/
let mapStateToProps = (state) =>{
    return{
        userPage: getUsers(state),
        pageSize: getPageSize(state),
        totalUsersCount: getTotalUsersCount(state),
        targetPage: getTargetPage(state),
        isFetching: getIsFetching(state),
        followingInProgress: getFollowingInProgress(state)
    }
}

export default connect(mapStateToProps, {
    setTargetPage: setTargetPageAC,
    getUsersThunkCreator: getUsersThunkCreator,
    followThunkCreator: followThunkCreator,
    unfollowThunkCreator: unfollowThunkCreator
    }
)(UsersAPIComponent)
import React from 'react'
import Preloader_s from './Preloader.module.css'

let Preloader = ()=>{
    return(
        <div className={Preloader_s.lds_dual_ring}>

        </div>
    )
}
export default Preloader
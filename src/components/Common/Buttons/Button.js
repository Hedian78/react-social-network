import React from "react"
import Button_s from "./Button.module.css"


export const ButtonLogout = (props) =>{
    return <button onClick={props.logoutThunkCreator} className={`${Button_s.btn} ${Button_s.first}`}>{props.value}</button>
}

export const ButtonAddPost = (props) =>{
    return <button onClick={props.addPost} className={Button_s.addPost}>{props.value}</button>
}
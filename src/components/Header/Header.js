import React from 'react';
import Header_s from './Header.module.css';
import {NavLink} from "react-router-dom";
import logo from './../../img/logo.png';
import {ButtonLogout} from "../Common/Buttons/Button";


const Header = (props)=>{
    return <header className={Header_s.header}>
        <div className={Header_s.logo}>
            <img src={logo} alt={"logo"}/>
            <span><b>CoffeeBreack</b></span>
        </div>
        <div className={Header_s.login}>

            {props.isAuth
                ? <div>{props.userData.login}<ButtonLogout logoutThunkCreator={props.logoutThunkCreator} value={"Log out"} /></div>
                : <NavLink to={'/login'}>Login</NavLink>
            }
        </div>
    </header>;
}

export default Header;

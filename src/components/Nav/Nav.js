import React from 'react';
import {NavLink} from "react-router-dom"
import Nav_s from './Nav.module.css';

const Nav = () => {
    return (
        <nav className={Nav_s.menu}>
            <NavLink className={Nav_s.fancy_hover} activeClassName={Nav_s.active} to="/profile">My profile</NavLink>
            <NavLink className={Nav_s.fancy_hover} activeClassName={Nav_s.active} to="/users">Users</NavLink>
            <NavLink className={Nav_s.fancy_hover} activeClassName={Nav_s.active} to="/friends">Friends</NavLink>
            <NavLink className={Nav_s.fancy_hover} activeClassName={Nav_s.active} to="/dialogs">Message</NavLink>
            {/*<NavLink className={Nav_s.fancy_hover} to="/photo">Photo</NavLink>
            <NavLink className={Nav_s.fancy_hover} to="/music">Music</NavLink>
            <NavLink className={Nav_s.fancy_hover} to="/video">Video</NavLink>*/}
        </nav>
    )
}

export default Nav;

import React from 'react';
import {useFormik} from 'formik';
import Login_s from './Login.module.css'
import {loginThunkCreator} from "../../redux/auth-reducer";
import {connect} from "react-redux";
import {Redirect} from "react-router-dom";



const Login = (props) => {
    // Pass the useFormik() hook initial form values and a submit function that will
    // be called when the form is submitted
    const formik = useFormik({
        initialValues: {
            email: '',
            password: '',
            rememberMe: false
        },
        onSubmit: values => {
            props.loginThunkCreator(values.email, values.password, values.rememberMe)
        },
    });
    if(props.isAuth) return <Redirect to={`/profile`} />
    return (
        <form className={Login_s.form} onSubmit={formik.handleSubmit}>
            <div className={Login_s.form_item}>
                <label htmlFor="email">email</label>
                <input
                    id="email"
                    name="email"
                    type="email"
                    onChange={formik.handleChange}
                    value={formik.values.email}
                />
            </div>
            <div className={Login_s.form_item}>
                <label htmlFor="password">password</label>
                <input
                    id="password"
                    name="password"
                    type="password"
                    onChange={formik.handleChange}
                    value={formik.values.password}
                />
            </div>
            <div className={Login_s.form_item_checkbox}>
                <input
                    id="rememberMe"
                    name="rememberMe"
                    type="checkbox"
                    onChange={formik.handleChange}
                    value={formik.values.rememberMe}
                />
                <label htmlFor="rememberMe"> Remember me</label>
            </div>

            <button className={Login_s.btn} type="submit">Submit</button>
        </form>
    );
};

let mapStateToProps = (state) => ({
    isAuth: state.auth.isAuth
})
export default connect(mapStateToProps,{loginThunkCreator})(Login)